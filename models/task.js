// This Contains WHAT objects are needed in our API

// Create the schema, model and export the file
const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
	name: String,	
	status: {
		type: String,
		default: "pending"
	}
});

// "module.exports" is a way Node JS to treat the value as a package that can be used bu other files
module.exports = mongoose.model("Task", taskSchema);



