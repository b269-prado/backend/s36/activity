// This Contains instructions on HOW your API will perform its intended tasks
// All the operations it can do will be placed in this file

const Task = require("../models/task");

// Controller function for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
};


// Controller function for creating a task
module.exports.createTask = (requestBody) => {
	
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if (error) {
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
};


// Controller function for deleting a task
/*
BUSINESS LOGIC
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((
		removedTask, err) => {
		if (err){
			console.log(err);
			return false;
		} else {
			return "Deleted Task!"
		}
	})
};

// [SECTION] Activity 36

/*
1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the /tasks/:id route using postman to get a specific task.
5. Create a route for changing the status of a task to complete.
6. Create a controller function for changing the status of a task to complete.
7. Return the result back to the client/Postman.
8. Process a PUT request at the /tasks/:id/complete route using postman to update a task.
*/


// Controller function for getting a specific task
module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
};

// Controller function for changing status of a specific task to complete
module.exports.changeTaskStatus = (taskId) => {
	return Task.findByIdAndUpdate(taskId, {status: 'complete'}, {new: true}).then(result => {
		return result;
	})
};



